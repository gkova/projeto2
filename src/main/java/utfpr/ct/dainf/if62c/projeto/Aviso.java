package utfpr.ct.dainf.if62c.projeto;

import java.text.SimpleDateFormat;
import java.util.TimerTask;

/**
 * Linguagem Java
 * @author 
 */
public class Aviso extends TimerTask{
    
    protected final Compromisso compromisso;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    public Aviso(Compromisso compromisso) {
       this.compromisso = compromisso;
    }
    
    @Override
    public void run() {
        long taviso = (compromisso.getData().getTime() - System.currentTimeMillis())/1000;
        System.out.println(compromisso +  " começa em " + taviso + "s.");
    }        
}
