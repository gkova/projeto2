package utfpr.ct.dainf.if62c.projeto;

import java.util.List;
import java.util.function.Consumer;

/**
 * Linguagem Java
 * @author 
 */
public class AvisoFinal extends Aviso {

    public AvisoFinal(Compromisso compromisso) {
        super(compromisso);
    }
    
    @Override
    public void run() {
        System.out.println(compromisso +  " começa  agora.");
        this.cancel();
        List<Aviso> av = compromisso.getAvisos();
        av.stream().forEach(new Consumer<Aviso>() {
            @Override
            public void accept(Aviso a) {
                a.cancel();
            }
        });
    }
}
